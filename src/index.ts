

export class RetryMyLocation {

    private count = 0;
    private watchId : number;
    private firstTime = true;
    private bestPosition : Position;

    private onErrorFn : (error: Error) => {};

    private fireError(error: Error) {
        this.onErrorFn(error);
    }

    onError = (fn) => {
        this.onErrorFn = fn;
    }

    constructor(private opts: LocationOptions){
    }

    async retrieve(skipWatch = false):Promise<Position> {
        let position = null;
        if (!skipWatch) {
            position = this.bestPosition = await this.watchPosition();
            // should have a position object..
            if (this.checkAccuracy(position)) {
                this.clearWatch();
                return position;
            }
        }


        while (this.count < this.opts.maximumRetry) {
            try {
                position = await this.currentPosition();
            } catch(err) {
                this.fireError(err);
                // reuse last position
            }
            if (this.checkAccuracy(position)) {
                this.clearWatch();
                return position;
            }
            this.count += 1;

            this.bestPosition = this.determineBestPosition(position);
        }

        this.clearWatch();
        return this.bestPosition;
    }

    private clearWatch() {
        if (this.watchId) {
            navigator.geolocation.clearWatch(this.watchId);
        }
    }

    private checkAccuracy(position: Position): boolean {
        if (position === null) {
            return false;
        }
        return (position.coords.accuracy <= this.opts.accuracy);
    }


    private determineBestPosition(position:Position): Position {
        const bestAccuracy = this.bestPosition ? this.bestPosition.coords.accuracy : Number.MAX_SAFE_INTEGER;
        if (position === null) {
            return null;
        }
        return (position.coords.accuracy < bestAccuracy) ? position : this.bestPosition;
    }

    private watchPosition(): Promise<Position> {
        return new Promise<Position>((resolve, reject) => {
            this.watchId = navigator.geolocation.watchPosition(pos => {
                if (this.firstTime) {
                    resolve(pos);
                    this.firstTime = false;
                }
            }, reject, this.opts);
        })
    }

    private currentPosition(): Promise<Position>{
        return new Promise<Position> ((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(resolve, reject, this.opts);
        })
    }
}


export class LocationOptions {
    maximumAge = 900;
    maximumRetry = 5;
    accuracy = 10;
    timeout = 20000;
    enableHighAccuracy = true;
}
